import React, { useEffect, useState } from "react";
import { StaticImage } from "gatsby-plugin-image";
import Layout from "../components/layout";

const push = () => {
  console.log("alert");
  alert("notif");
};
const IndexPage = () => {
  const [message, setMessage] = useState("");
  useEffect(async () => {
    if ("serviceWorker" in navigator) {
      const registration = await navigator.serviceWorker.getRegistration();
      if (registration) {
        setMessage("Service worker was registered on page load");
      } else {
        setMessage("No service worker is currently registered");
      }
    } else {
      setMessage("Service workers API not available");
    }
    console.log("coucou", navigator);
  });
  return (
    <Layout>
      <h1>Hi people</h1>
      <p>Welcome to your new Gatsby site.</p>
      <p>Registrable message : </p>
      <p>{message}</p>
      <StaticImage
        src="../images/gatsby-astronaut.png"
        width={300}
        quality={95}
        formats={["auto", "webp", "avif"]}
        alt="A Gatsby astronaut"
        style={{ marginBottom: `1.45rem` }}
      />
      <button onClick={push}>Coucou</button>
    </Layout>
  );
};

export default IndexPage;
