if (process.env.STAGING) {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}.staging`,
  });
} else {
  require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
  });
}

module.exports = {
  pathPrefix: process.env.CI_PROJECT_NAME || process.env.PATH_PREFIX || "/",
  siteMetadata: {
    title: `Maitre Queux`,
    description: `Deviens maître de ta cuisine en préparant de déliceux repas pour la semaine`,
    author: `@mrdant`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Progressive web app`,
        short_name: `PWA`,
        start_url: `/`,
        description:
          "Progressive Web App that lists games submitted to the A-Frame category in the js13kGames 2017 competition.",
        background_color: `#663399`,
        // This will impact how browsers show your PWA/website
        // https://css-tricks.com/meta-theme-color-and-trickery/
        theme_color: `#663399`,
        display: `fullscreen`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};
